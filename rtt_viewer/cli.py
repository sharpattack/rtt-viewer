from pathlib import Path
import time
import click
from rtt_viewer.jlink import Interface, RttRecorder


@click.group()
def app():
    pass


@app.command()
@click.argument("device", nargs=1)
@click.option(
    "-c",
    "--channel",
    type=click.INT,
    default=0,
    help="UP Channel to listen to",
    show_default=True,
)
@click.option(
    "--iface",
    type=click.Choice(["JTAG", "SWD"], case_sensitive=False),
    default="SWD",
    help="Type of interface to use to debug the device",
    show_default=True,
)
@click.option(
    "--block-address",
    type=click.INT,
    default=None,
    help="Address of the RTT block. Use None for auto",
    show_default=True,
)
@click.option(
    "--speed",
    type=click.INT,
    default=None,
    help="Communication speed. Use None for auto",
    show_default=True,
)
@click.option(
    "--library",
    type=click.Path(
        exists=True, file_okay=True, dir_okay=False, resolve_path=True, path_type=Path
    ),
    default=None,
    help="Path to the JLink dll/library. Use None for auto",
    show_default=True,
)
@click.option(
    "--serial-no",
    type=click.INT,
    default=None,
    help="Serial number of the JLink. Use None for auto",
    show_default=True,
)
@click.option(
    "--ip-addr",
    type=click.STRING,
    default=None,
    help="IP address of a JLink server. Use None for auto",
    show_default=True,
)
def plain(
    channel: int,
    device: str,
    iface: str,
    block_address: int | None,
    speed: int | None,
    library: Path | None,
    serial_no: int | None,
    ip_addr: str | None,
):
    """Plain text one channel logs, DEVICE is the MCU name (eg: STM32F446ZE)"""
    interface: Interface = getattr(Interface, iface.upper())
    jlink = RttRecorder(
        device.upper(),
        interface=interface,
        block_address=block_address,
        speed=speed,
        lib_path=library,
        serial_no=serial_no,
        ip_address=ip_addr,
        rtt_log_channel_list=(
            channel,
        ),  # Only record one channel so we can open this utility multiple times
    )
    jlink.connect()
    if channel >= jlink.channels:
        jlink.close()
        raise ValueError(
            f"The device only supports {jlink.channels} channels, {channel} is an invalid value"
        )
    while True:
        print(jlink.logs_incremental(channel), end="")
        time.sleep(0.1)
