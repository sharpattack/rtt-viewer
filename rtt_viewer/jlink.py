import time
import os
import pylink
import pylink.library
import threading
import logging

from io import StringIO
from pathlib import Path
from pylink.enums import JLinkInterfaces, JLinkHost
from enum import IntEnum


class Interface(IntEnum):
    JTAG = JLinkInterfaces.JTAG
    SWD = JLinkInterfaces.SWD


class RttRecorder:

    def __init__(
        self,
        device: str,
        interface: Interface = Interface.SWD,
        block_address: int | None = None,  # None is Auto
        speed: int | None = None,  # None is Auto
        lib_path: Path | None = None,  # None is Auto
        serial_no: int | None = None,  # None is Auto
        ip_address: str | None = None,  # None is Auto
        rtt_log_channel_list: (
            tuple[int] | None
        ) = None,  # List of rtt channels to record. None mean everything
    ):
        jlink_args = {}

        # Load JLink library
        if lib_path is not None:
            try:
                lib = pylink.library.Library(Path(lib_path))
            except FileNotFoundError as e:
                raise type(e)(f"Could not load JLink library from {lib_path}") from e

            jlink_args["lib"] = lib

        self._jlink = pylink.JLink(**jlink_args)

        self._interface = interface
        self._serial_no = serial_no
        self._ip_address = ip_address

        # RTT buffers
        self._rtt_logs = []
        self._nb_channels = 0

        # Cursor
        self._pos = []

        # RTT thread
        self._rtt_configured = threading.Event()
        self._rtt_thread = None
        self._clear_logs = threading.Event()

        self._device = device
        self._speed = speed if speed is not None else "auto"
        self._rtt_block_address = block_address

        self._rtt_log_channel_list = (
            rtt_log_channel_list if rtt_log_channel_list is not None else None
        )

    @property
    def channels(self) -> int:
        return self._nb_channels

    def _rtt_reader(self, is_ready: threading.Event, read_size: int = 1024):

        def get_nb_channels(timeout: int = 3):
            start = time.monotonic()
            while True:
                if time.monotonic() - start > timeout:
                    raise TimeoutError
                try:
                    return self._jlink.rtt_get_num_up_buffers()
                except pylink.JLinkRTTException:
                    pass

        # Start RTT logs
        self._jlink.rtt_start(self._rtt_block_address)
        nb_channels = get_nb_channels()
        while len(self._rtt_logs) < nb_channels:
            self._rtt_logs.append("")

        while len(self._pos) < nb_channels:
            self._pos.append(0)

        if self._rtt_log_channel_list is None:
            self._rtt_log_channel_list = tuple(range(nb_channels))
        self._nb_channels = nb_channels
        is_ready.set()

        # Start reading loop
        EMPTY_MESSAGE_THREASHOLD = (
            3  # Number of allowed empty messages before resetting logs
        )
        empty_messages_counter = 0

        while self._rtt_configured.is_set():
            time.sleep(0.1)

            # Clear logs
            if not self._clear_logs.is_set():
                for i in len(self._rtt_logs):
                    self._rtt_logs[i] = ""

            got_bytes = False
            for i in self._rtt_log_channel_list:
                try:
                    rtt_bytes = self._jlink.rtt_read(i, read_size)
                except pylink.JLinkRTTException as e:
                    logging.warning(f"Error reading RTT logs {e}")
                    continue
                else:
                    if rtt_bytes:
                        try:
                            self._rtt_logs[i] += bytearray(rtt_bytes).decode("utf-8")
                        except UnicodeDecodeError as e:
                            logging.warning(f"Error decoding {e}")
                        else:
                            got_bytes = True
            if got_bytes:
                empty_messages_counter = 0
            else:
                empty_messages_counter += 1

            # RTT logs might get stuck if the device resets a lot
            # We reconfigure RTT logs when we suspects this to happen
            if empty_messages_counter < EMPTY_MESSAGE_THREASHOLD:
                continue

            logging.debug("Restarting rtt logs")
            try:
                self._jlink.rtt_stop()
                self._jlink.rtt_start(self._rtt_block_address)
            except pylink.JLinkRTTException as e:
                logging.error(f"Could not reset RTT logs in thread {e}, disabling logs")
                self._stop_rtt()
                return

            empty_messages_counter = 0

    def connect(self):
        """Connect to the configured device and start recording logs"""

        if self._rtt_configured.is_set():
            return

        if self._jlink.target_connected():
            logging.info("Already connected to the device")
            return

        try:
            self._jlink.open(serial_no=self._serial_no, ip_addr=self._ip_address)
        except pylink.JLinkException as e:
            raise type(e)("Could not find any jlink")

        self._jlink.set_tif(self._interface)
        try:
            self._jlink.connect(self._device, speed=self._speed)
        except pylink.JLinkException as e:
            self._jlink.close()
            raise type(e)("Could not connect to the device")

        self._start_rtt()

    def _start_rtt(self):

        is_ready = threading.Event()
        self._clear_logs.set()
        self._rtt_configured.set()
        self._rtt_thread = threading.Thread(
            target=self._rtt_reader,
            args=(is_ready,),
            daemon=True,
        )
        self._rtt_thread.start()

        if not is_ready.wait(timeout=4):
            raise TimeoutError("RTT logs initialization timed out")

    def _stop_rtt(self):
        self._rtt_configured.clear()

        if self._rtt_thread is None:
            return

        self._rtt_thread.join(timeout=1)
        self._rtt_thread = None

    def close(self):
        """Stop log recording and close connection to the configured device"""
        self._stop_rtt()
        if self._jlink.target_connected():
            self._jlink.close()

    def clear(self):
        if self._rtt_thread is None:
            return

        self._clear_logs.clear()
        if not self._clear_logs.wait(timeout=1):  # Wait for log clearing
            raise TimeoutError("Could not clear RTT logs")

    def logs_incremental(self, channel: int = 0) -> str:
        if channel not in self._rtt_log_channel_list:
            raise ValueError(
                f"Invalid channel {channel}, recorded channels are {self._rtt_log_channel_list}"
            )
        logs = self._rtt_logs[channel]
        end = len(logs)
        start = min(self._pos[channel], end)

        self._pos[channel] = end
        return logs[start:]

    def logs(self, channel: int = 0) -> str:
        if channel not in self._rtt_log_channel_list:
            raise ValueError(
                f"Invalid channel {channel}, recorded channels are {self._rtt_log_channel_list}"
            )
        return self._rtt_logs[channel]
